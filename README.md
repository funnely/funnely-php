# Funnely SDK for PHP #

This is [Funne.ly](http://www.funne.ly)'s SDK for connecting your online store product, categories and orders feeds.

 Installation
---------------

The PHP SDK is distributed as an independent library. After downloading the SDK, place it in a directory where your online store can include it.

In order to use the SDK you need to have the **curl** extension installed. Please refer to the [PHP manual](http://php.net/manual/en/curl.installation.php) for further reference. Usually for debian based linux installations, the way to install the php curl library is:


```
#!shell

$ sudo apt-get install php5-curl
```


 Setup
---------------

To include the Funne.ly SDK in your project, make sure that you require the funnely.php file inside the /lib directory. Assuming that you placed the sdk under your project's /lib directory:


```
#!php

require_once('lib/funnely-php/lib/funnely.php');

```



In order to initialize the SDK, you need your API Key and Access Token. You can find these by logging in to Funne.ly and navigating to the Online Store section in the [Social Credentials](http://go.funne.ly/#/social-credentials) page.

Once you have this information, initialize the sdk with the following line

```
#!php

$funnely = new \Funnely\SDK($api_key, $access_token)
```

 Data Mapping
-------------------

Regardless of the SDK mode selected, the data from your online store must be mapped to Funne.ly's feed structure so it can be properly processed. In this section, you will find a sample of how your data should be mapped. The format for all the feeds is JSON.

### Products feed ###

The products feed is a list of the products that you want Funne.ly to have access to. These are usually your active products.

```
#!JSON

[  
  {  
    "id":13192,
    "name":"LG Nexus 4",
    "description":"This is a great phone!",
    "handle":"/products/lg-nexus-4",
    "stock_remaining":8,
    "price":423.00,
    "original_price": 500.00,
    "currency":"USD",
    "published_at":"2014-10-21",
    "main_image":"/assets/images/product-01.jpg",
    "images":[  
      "/assets/images/product-01.jpg",
      "/assets/images/product-02.jpg"
    ]
  },
  {  
    "id":13193,
    "name":"Iphone 5s",
    "description":"This is another great phone!",
    "handle":"/products/iphone-5s",
    "stock_remaining":2,
    "price":512.00,
    "currency":"USD",
    "published_at":"2014-10-21",
    "main_image":"/assets/images/product-11.jpg",
    "images":[  
      "/assets/images/product-11.jpg",
      "/assets/images/product-12.jpg"
    ]
  }
]

```
Notes:

* The '**id**' field must be unique. More than one element with the same id will not be accepted.
* The '**description**' field accepts both plain text and HTML
* All URLs can be either relative (i.e. '/assets/images/product-01.jpg') or full (i.e. 'http://www.yourstore.com/assets/images/product-01.jpg'). In case of relative URLs, Funne.ly will prepend the Store URL defined when connecting your online store to Funne.ly. If you want to change this URL, go to the online store section of Funne.ly's (Social Credentials)(http://go.funne.ly/#/social-credentials).
* The '**price**' field represents the final price of the product (after discounts). Format is dot for decimal separator and no thousand separator (i.e. 4200.50 is ok, but 4,200.50 is not)
* The '**orginal_price**' field represents the price of the product before discount. format is dot for decimal separator and no thousand separator (i.e. 4200.50 is ok, but 4,200.50 is not). This field is optional if there are no discounts applied.
* The '**currency**' format follows the ISO-4127 currency format
* The '**published_at**' field is the date your product was created, or was active for the last time

### Categories Feed ###

The categories feed is a list of the product categories that you want Funne.ly to have access to. These are usually your active product categories.


```
#!json

[  
  {  
    "id":600,
    "name":"Smartphones",
    "description":"The greatest Smartphones",
    "parent_id":0,
    "product_ids":[  
      13192,
      13193
    ],
    "handle":"/categories/smartphones",
    "published_at":"2014-01-15",
    "main_image":"/assets/images/smartphones.jpg",
    "images":[  
      "/assets/images/smartphones.jpg",
      "/assets/images/smartphones_2.jpg"
    ]
  },
  {  
    "id":601,
    "name":"Apple Smartphones",
    "description":"The greatest Smartphones",
    "parent_id":600,
    "product_ids":[  
      13193
    ],
    "handle":"/categories/apple-smartphones",
    "published_at":"2014-01-16",
    "main_image":"/assets/images/apple_smartphones.jpg",
    "images":[  
      "/assets/images/apple_smartphones.jpg",
      "/assets/images/apple_smartphones_2.jpg"
    ]
  }
]

```
Notes:

* The '**id**' field must be unique. More than one element with the same id will not be accepted.
* The '**description**' field accepts both plain text and HTML
* The '**parent_id**' is the category id for the parent category. This field is not required. If 0 is set as parent_id, Funne.ly will asume that this category does not have a parent category.
* If you don't have images for a category, then the '**images**' and '**main_image**' fields are not required.
* The '**product_ids**' field is a list of the product ids that are inside that category. This is not a required field. Funne.ly uses this for retargeting if present.
* All URLs can be either relative (i.e. '/assets/images/smartphones.jpg') or full (i.e. 'http://www.yourstore.com/assets/images/smartphones.jpg'). In case of relative URLs, Funne.ly will prepend the Store URL defined when connecting your online store to Funne.ly. If you want to change this URL, go to the online store section of Funne.ly's (Social Credentials)(http://go.funne.ly/#/social-credentials).
* The '**published_at**' field is the date your product was created, or was active for the last time

### Customers Feed ###

The customers feed is a list of all your customers. Funne.ly uses your customers feed to create and maintain relevant audiences.


```
#!json

[
 {
   "id": 3000,
   "name": "Jack Smith",
   "email": ["jsmith@gmail.com", "jacksmith@yahoo.com"]
 },
 {
   "id": 3001,
   "name": "Robert White",
   "email": "robert@site.com"
 }
]
```
Notes:

* The '**id**' field must be unique. More than one element with the same id will not be accepted.
* The '**email**' field accepts either a string with the customer's email, or a list (array) of all the customer's emails that you have registered.

### Orders Feed ###

The orders feed is a list of all your online store orders. Funne.ly uses this information to properly target and optimize your campaigns.


```
#!json

[  
  {  
    "id":15000,
    "amount":1447.00,
    "currency":"USD",
    "customer_id":3000,
    "created_at":"2014-10-20",
    "products":[  
      {  
        "id":13192,
        "quantity":1,
        "amount":423.00,
        "currency": "USD"
      },
      {  
        "id":13193,
        "quantity":2,
        "amount":1024.00,
        "currency": "USD"
      }
    ]
  },
  {  
    "id":15001,
    "amount":423.00,
    "currency":"USD",
    "customer_id":3001,
    "created_at":"2014-10-21",
    "products":[  
      {  
        "id":13192,
        "quantity":1,
        "amount":423.00,
        "currency": "USD"
      }
    ]
  }
]
```
Notes:

* The '**id**' field must be unique. More than one element with the same id will not be accepted.
* The '**amount**' format is dot for decimal separator and no thousand separator (i.e. 4200.50 is ok, but 4,200.50 is not)
* The '**currency**' format follows the ISO-4127 currency format
* The '**customer_id**' field is the id for the customer who placed the order
* The '**created_at**' field is the date the order was placed

### Using the SDK ###

Here are some use cases where you would Funnely's SDK

* You have just signed up with Funne.ly and you want to send all your feeds data to be analyzed and used to create your campaigns.
* You want to feed funne.ly new entries in any of your feeds
* You want to update elements in your feed (i.e. products or categories that have changed, users that have changed their email address)

We will go through each of these scenarios:

### Initial data load ###

When you sign up with Funne.ly, you will need to make an initial data load of all your relevant feeds (products, categories, orders and customers). If any of these feeds is not updated, then Funne.ly assumes that they are not relevant for creating and optimizng your campaigns (i.e. you have just started selling online, and you don't have any customers or orders, or your online store does not support categories).  

If you don't have any customers or orders yet, don't worry! Feeds can be updated at any time, and Funne.ly will start using the information as soon as it is available.

Here's an example on how to use the SDK to send an initial data feed of your products. The same concept applies to the categories, customers and orders feeds.
```
#!php
<?php
function update_funnely_feeds() {
  
  $funnely = new \Funnely\SDK($api_key, $access_token);
  # We assume that $products is a model for the product entity
  $my_products = $products->find_by_status("active");

  $my_formatted_products = array();
  foreach($my_products as $product) {
    # Map your product data to funnely's structure and append to $my_formatted_products
  }

  try {
    $funnely.update_products(json_encode($my_formatted_products))
  } catch(\Funnely\Exception  $fe) {
     # Something went wrong
     echo $fe;
  }
  
}
?>
```

The methods that are available to update feeds are:

* **\Funnely\SDK::update_products($products)** Which updates the products feed. The '$products' parameter is 
the mapped list of products in JSON format
* **\Funnely\SDK::update_categories($categories)** Which updates the categories feed. The '$categories' parameter is the mapped list of categories in JSON format
* **\Funnely\SDK::update_customers($customers)** Which updates the customers feed. The '$customers' parameter is the mapped list of customers in JSON format
* **\Funnely\SDK::update_orders($orders)** Which updates the orders feed. The '$orders' parameter is the mapped list of orders in JSON format

### Adding new elements to your feed ###

In order to make the data feed process more efficient, instead of sending all your feed data every day, you can just send data as it is created. For example, you can add a new order to the orders feed, or a new product to the products feed as it is created or published.

In the following example, we will assume that you have a model for your orders, and that this code is placed within your order creation method


```
#!php
<?php
function create() {
  # ..Code for creating your new order..
  $order->save()
  $funnely = new \Funnely\SDK($api_key, $access_token)
  $formatted_order = new stdClass();
  # ..Code for mapping your order's data to funne.ly structure and assign to the $formatted_order variable
  try {
    $funnely->update_orders(json_encode(array($formatted_order)));
  } catch(\Funnely\Exception $fe) {
     # Something went wrong
     echo $fe;
  }
}
?>
```
Notice that the same methods are used for each of the feeds, but for this example instead of sending the whole list of orders, you are just sending the new one, updating your feed incrementally. This can be done for any of your relevant feeds.

Also notice that you still need to send a list to the update_products() metod.

If you don't have access to hook into the code of your online store to do this, you can always send the full list of data periodically, or create a script that will send all the new elements from your feeds periodically.

### Updating your feeds ###

Once you have made the initial data load, and are able to add new elements to your feed, you'll need to update  your feeds' elements (i.e. When you add or remove an image from a product, or change its price).

For this example, we will assume that you have a model for your Products, and that there is an update method for said model.


```
#!php
<?php
function update() {
   # ..code to update your product
   $product.save();
   $funnely = new \Funnely\SDK($api_key, $access_token);
   $formatted_product = new stdClass();
   # ..code to map your product's data to funne.ly structure and assign to the $formatted_product variable
   try {
     funnely.update_products(json_encode(array(formatted_product])));
   } catch(\Funnely\Exception $fe) {
     # Something went wrong.
     echo $fe;
   }
   
}
?>
```

Notice that the same methods are used for each of the feeds, but for this example instead of sending the whole list of products, you are just sending the one that has been updated. This can be done for any of your relevant feeds. Remember that the id of the element that you're updating must match the one that you have previosly loaded either in the initial data load, or when a new element of the feed was created.

Also notice that you still need to send a list to the update_products() metod, even if the list has one element.

### Removing elements from the feed ###

Finally, you might want to remove elements from your feeds. The SDK exposes methods to remove a list of elements from any of your feeds, or to clear a feed entierly.

Here's an example on how to remove a list of elements from your products feed. The same can be done with the remove methods for the other feeds:

```
#!php
<?php
function destroy() {
  # ..code to remove a product
  $funnely = new \Funnely\SDK($api_key, $access_token);
  try {
    $funnely->remove_products(json_encode(array($product.id)))
    $product->destroy();
  } catch (\Funnely\Exception $fe) {
     # Something went wrong.
     echo $fe;  
  }
}
?>
```
Here's a list of the remove methods that are available for the different feeds:

* **\Funnely\SDK::remove_products($products)** Which removes the products from the products feed, that match the ids passed in the '$products' parameter. The '$products' paramater is a list of ids, in JSON format
* **\Funnely\SDK::remove_categories($categories)** Which removes the categories from the categories feed, that match the ids passed in the '$categories' parameter. The '$categories' paramater is a list of ids, in JSON format
* **\Funnely\SDK::remove_customers($customers)** Which removes the customers from the customers feed, that match the ids passed in the '$customers' parameter. The '$customers' paramater is a list of ids, in JSON format
* **\Funnely\SDK::remove_orders($orders)** Which removes the orders from the orders feed, that match the ids passed in the '$orders' parameter. The '$orders' paramater is a list of ids, in JSON format

You can implement these methods whenever an element is phisically removed from your online store, or whenever you want to remove an element from any of the feeds (i.e., when a product is deactivated and you don't need funne.ly to use it)

Here's an example on how to clear a feed entirely. You can use this if for some reason you want Funne.ly to stop using the information from a feed.

```
#!php
<?php
function clear() {
  $funnely = new \Funnely\SDK($api_key, $access_token);
  try {
    funnely.clear_products()
  } catch(\Funnely\Exception $fe) {
     echo $fe;
  }
}
?>
```
Here's a list of the clear methods that are available for the different feeds:

* **\Funnely\SDK::clear_products()** Which clears the products feed
* **\Funnely\SDK::clear_categories()** Which clears the categories feed
* **\Funnely\SDK::clear_customers()** Which clears the customers feed
* **\Funnely\SDK::clear_orders()** Which clears the orders feed