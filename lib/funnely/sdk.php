<?php
namespace Funnely;

class SDK {
  private $api_key;
  private $access_token;

	function __construct($api_key, $access_token) {
    $this->api_key = $api_key;
    $this->access_token = $access_token;
	}

  # Product Feed methods
  function update_products($products) {
    $this->make_call('/api/v1/open/feed/products', $products, "post");
  }

  function remove_products($products) {
    $this->make_call('/api/v1/open/feed/products', $products, "delete");
  }

  function clear_products() {
    $this->make_call('/api/v1/open/feed/products', '[]', "delete");
  }

  # Category Feed methods
  function update_categories($categories) {
    $this->make_call('/api/v1/open/feed/categories', $categories, "post");
  }

  function remove_categories($categories) {
    $this->make_call('/api/v1/open/feed/categories', $categories, "delete");
  }

  function clear_categories() {
    $this->make_call('/api/v1/open/feed/categories', '[]', "delete");
  }

  # Customer Feed methods
  function update_customers($customers) {
    $this->make_call('/api/v1/open/feed/customers', $customers, "post");
  }

  function remove_customers($customers) {
    $this->make_call('/api/v1/open/feed/customers', $customers, "delete");
  }

  function clear_customers() {
    $this->make_call('/api/v1/open/feed/customers', '[]', "delete");
  }

  # Order Feed methods
  function update_orders($orders) {
    $this->make_call('/api/v1/open/feed/orders', $orders, "post");
  }

  function remove_orders($orders) {
    $this->make_call('/api/v1/open/feed/orders', $orders, "delete");
  }

  function clear_orders() {
    $this->make_call('/api/v1/open/feed/orders', '[]', "delete");
  }   

  private function make_call($endpoint, $parameters, $method = "post") {
    $url = "http://beta-api.funne.ly" . $endpoint .
     "?api_key=" . $this->api_key . "&access_token=" . $this->access_token;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($parameters))                                                                       
    );        
    if($method == "post") {
      curl_setopt($ch, CURLOPT_POST, true);
    } else if ($method == "delete") {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    }
    
    $response = curl_exec($ch);
    $status = curl_getinfo($ch,CURLINFO_HTTP_CODE); 
    return $this->analyze_response($response, $status);
  }

  private function analyze_response($response, $status) {
    $response = json_decode($response);
    $accepted_statuses = [200, 201];

    if($response->error_messages) {
      $response->message = implode("\n", $response->error_messages);
    }
    $response->code = $status;

    if(in_array($status, $accepted_statuses)) {
      return true;
    } else {
      throw new \Funnely\Exception($response->message, $response->code);
    }
    return true;
  }
}
?>